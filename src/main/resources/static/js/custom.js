//accept string in format dd/mm/yyyy
function toDate(dateStr) {
    var parts = dateStr.split("/");
    //yyyy, mm, dd: month in 0-based
    return new Date(parts[2], parts[1] - 1, parts[0]);
}