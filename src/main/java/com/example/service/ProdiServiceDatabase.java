package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.ProdiMapper;
import com.example.model.ProdiModel;
import com.example.model.ServiceDatabaseEnum;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService{
	@Autowired
    private ProdiMapper prodiMapper;
	
	@Override
	public List<ProdiModel> getAllProdi() {
		log.info ("get all prodi");
		return prodiMapper.getAllProdi();
	}

	@Override
	public ProdiModel getProdi(String idProdi) {
		log.info ("get prodi with kode_prodi {}", idProdi);
		return prodiMapper.getProdi(idProdi);
	}

	@Override
	public List<ProdiModel> getAllProdiByUniv(String kodeUniv) {
		log.info ("get all prodi with kode_univ {}", kodeUniv);
		return prodiMapper.getAllProdiByUniv(kodeUniv);
	}

	@Override
	public ServiceDatabaseEnum addProdi(ProdiModel prodi) {
		log.info ("add new prodi: {}, {}, {}", prodi.getKodeUniv(), prodi.getKode(), prodi.getNama());
		if (prodiMapper.getProdi(prodi.getKode()) != null) {
			//existing prodi's nomor
			log.info ("add prodi failed because duplicate prodi kode");
			return ServiceDatabaseEnum.FAILED_INSERT_PRODI_DUPLICATE_KODE;
		}else if (prodi.getKode().length() > 6) {
			return ServiceDatabaseEnum.ERROR_PRODI_KODE_CHARLIMIT;
		}else if (prodi.getKodeUniv().length() > 2) {
			return ServiceDatabaseEnum.ERROR_PRODI_KODEUNIV_CHARLIMIT;
		}else if (prodi.getNama().length() > 100) {
			return ServiceDatabaseEnum.ERROR_PRODI_NAMA_CHARLIMIT;
		}
		
		prodiMapper.addProdi(prodi);
		
		return ServiceDatabaseEnum.SUCCESS;
	}

	@Override
	public void deleteProdi(String kodeProdi, String kodeUniv) {
		log.info ("delete prodi with kode_prodi {} and kode_univ {}", kodeProdi, kodeUniv);
		prodiMapper.deleteProdi(kodeProdi, kodeUniv);
	}

	@Override
	public ServiceDatabaseEnum updateProdi(ProdiModel prodi) {
		log.info ("update prodi with kode_prodi {}, kode_univ {}, into nama {}", 
				prodi.getKode(), prodi.getKodeUniv(), prodi.getNama());
		if (prodi.getKode().length() > 6) {
			return ServiceDatabaseEnum.ERROR_PRODI_KODE_CHARLIMIT;
		}else if (prodi.getKodeUniv().length() > 2) {
			return ServiceDatabaseEnum.ERROR_PRODI_KODEUNIV_CHARLIMIT;
		}else if (prodi.getNama().length() > 100) {
			return ServiceDatabaseEnum.ERROR_PRODI_NAMA_CHARLIMIT;
		}
		
		prodiMapper.updateProdi(prodi);
		
		return ServiceDatabaseEnum.SUCCESS;
	}

}
