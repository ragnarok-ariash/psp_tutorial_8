package com.example.service;

import java.util.List;

import com.example.model.ProdiModel;
import com.example.model.ServiceDatabaseEnum;

public interface ProdiService {
	List<ProdiModel> getAllProdi();
	List<ProdiModel> getAllProdiByUniv(String kodeUniv);
	ProdiModel getProdi(String idProdi);
	ServiceDatabaseEnum addProdi(ProdiModel prodi);
	void deleteProdi(String kodeProdi, String kodeUniv);
	ServiceDatabaseEnum updateProdi(ProdiModel prodi);
}
