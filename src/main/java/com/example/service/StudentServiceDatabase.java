package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.StudentMapper;
import com.example.model.ServiceDatabaseEnum;
import com.example.model.StudentModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StudentServiceDatabase implements StudentService{
	@Autowired
    private StudentMapper studentMapper;
	
	@Override
	public StudentModel selectStudent(String nomor) {
		log.info ("select student with nomor {}", nomor);
        return studentMapper.selectStudent (nomor);
	}

	@Override
	public ServiceDatabaseEnum addStudent(StudentModel student) {
		log.info ("add student with nomor peserta {}, nama {}, tanggal lahir {}, kode prodi {}", 
				student.getNomor(), student.getNama(), student.getTglLahir(), student.getKodeProdi());
		if (studentMapper.selectStudent(student.getNomor()) != null) {
			//existing student's nomor peserta
			log.info ("add student failed because duplicate nomor_peserta");
			return ServiceDatabaseEnum.FAILED_INSERT_PESERTA_DUPLICATE_KODE;
		}if (student.getNomor().length() > 10) {
			return ServiceDatabaseEnum.ERROR_PESERTA_NOMOR_CHARLIMIT;
		}else if (student.getNama().length() > 30) {
			return ServiceDatabaseEnum.ERROR_PESERTA_NAMA_CHARLIMIT;
		}else if (student.getKodeProdi()!=null && student.getKodeProdi().length() > 6) {
			return ServiceDatabaseEnum.ERROR_PESERTA_KODEPRODI_CHARLIMIT;
		}
		
		studentMapper.addStudent(student);
		return ServiceDatabaseEnum.SUCCESS;
	}

	@Override
	public void deleteStudent(String nomorPeserta) {
		log.info ("delete student with nomor peserta {}", nomorPeserta);
		studentMapper.deleteStudent(nomorPeserta);
	}

	@Override
	public ServiceDatabaseEnum updateStudent(StudentModel student) {
		log.info ("update student with nomor peserta {}, nama {}, tanggal lahir {}, kode prodi {}", 
				student.getNomor(), student.getNama(), student.getTglLahir(), student.getKodeProdi());
		
		if (student.getNomor().length() > 10) {
			return ServiceDatabaseEnum.ERROR_PESERTA_NOMOR_CHARLIMIT;
		}else if (student.getNama().length() > 30) {
			return ServiceDatabaseEnum.ERROR_PESERTA_NAMA_CHARLIMIT;
		}else if (student.getKodeProdi().length() > 6) {
			return ServiceDatabaseEnum.ERROR_PESERTA_KODEPRODI_CHARLIMIT;
		}
		
		studentMapper.updateStudent(student);
		return ServiceDatabaseEnum.SUCCESS;
	}

	@Override
	public StudentModel selectStudentWithUnivProdi(String nomor) {
		log.info ("select student with nomor {}", nomor);
        return studentMapper.selectStudentWithUnivProdi (nomor);
	}

}
