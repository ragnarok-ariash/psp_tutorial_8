package com.example.service;

import java.util.List;

import com.example.model.ServiceDatabaseEnum;
import com.example.model.UniversityModel;

public interface UniversityService {
	List<UniversityModel> getAllUniversities();
	UniversityModel getUniversity(String id);
	UniversityModel getUniversityParticipants(String id);
	ServiceDatabaseEnum addUniversity(UniversityModel university);
	void deleteUniversity(String kodeUniv);
	ServiceDatabaseEnum updateUniversity(UniversityModel university);
}
