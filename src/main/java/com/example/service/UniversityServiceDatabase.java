package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UniversityMapper;
import com.example.model.ServiceDatabaseEnum;
import com.example.model.UniversityModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UniversityServiceDatabase implements UniversityService{
	@Autowired
    private UniversityMapper universityMapper;
	
	@Override
	public List<UniversityModel> getAllUniversities() {
		log.info ("get all universities");
        return universityMapper.getAllUniversities();
	}

	@Override
	public UniversityModel getUniversity(String id) {
		log.info ("get university with kode_univ: {}", id);
		return universityMapper.getUniversity(id);
	}

	@Override
	public UniversityModel getUniversityParticipants(String id) {
		log.info ("get university participants with kode_univ: {}", id);
		return universityMapper.getUniversityParticipants(id);
	}

	@Override
	public ServiceDatabaseEnum addUniversity(UniversityModel university) {
		if (universityMapper.getUniversity(university.getKode()) != null) {
			//existing university's kode
			log.info ("add university failed because duplicate kode_univ");
			return ServiceDatabaseEnum.FAILED_INSERT_UNIV_DUPLICATE_KODE;
		}else if (university.getKode().length() > 2) {
			return ServiceDatabaseEnum.ERROR_UNIV_KODE_CHARLIMIT;
		}else if (university.getNama().length() > 100) {
			return ServiceDatabaseEnum.ERROR_UNIV_NAMA_CHARLIMIT;
		}else if (university.getUrl().length() > 100) {
			return ServiceDatabaseEnum.ERROR_UNIV_URL_CHARLIMIT;
		}
		
		universityMapper.addUniversity(university);
		return ServiceDatabaseEnum.SUCCESS;
		
	}

	@Override
	public void deleteUniversity(String kodeUniv) {
		log.info ("delete university with kode_univ: {}", kodeUniv);
		universityMapper.deleteUniversity(kodeUniv);
	}

	@Override
	public ServiceDatabaseEnum updateUniversity(UniversityModel university) {
		log.info("updae university {}: {}, {}", 
				university.getKode(), university.getNama(), university.getUrl());
		if (university.getKode().length() > 2) {
			return ServiceDatabaseEnum.ERROR_UNIV_KODE_CHARLIMIT;
		}else if (university.getNama().length() > 100) {
			return ServiceDatabaseEnum.ERROR_UNIV_NAMA_CHARLIMIT;
		}else if (university.getUrl().length() > 100) {
			return ServiceDatabaseEnum.ERROR_UNIV_URL_CHARLIMIT;
		}
		
		universityMapper.updateUniversity(university);
		return ServiceDatabaseEnum.SUCCESS;
	}



}
