package com.example.service;

import com.example.model.ServiceDatabaseEnum;
import com.example.model.StudentModel;

public interface StudentService {
	StudentModel selectStudent(String nomor);
	StudentModel selectStudentWithUnivProdi(String nomor);
	ServiceDatabaseEnum addStudent(StudentModel student);
	void deleteStudent(String nomorPeserta);
	ServiceDatabaseEnum updateStudent(StudentModel student);
}
