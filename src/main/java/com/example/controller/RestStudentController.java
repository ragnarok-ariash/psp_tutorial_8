package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.StudentModel;
import com.example.service.ProdiService;
import com.example.service.StudentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class RestStudentController {
	@Autowired
	StudentService studentDAO;
	
	@Autowired
	ProdiService prodiDAO;

	@RequestMapping(value="/rest/peserta", method= RequestMethod.GET)
	public StudentModel peserta(
			Model model, 
			@RequestParam(value = "nomor", required = false, defaultValue = "0") String nomor){
		log.info("get peserta detail {}",nomor);
		StudentModel student = studentDAO.selectStudent(nomor);
		
	    return student;
	}
	
	
}
