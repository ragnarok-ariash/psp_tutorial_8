package com.example.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.HeaderMenuModel;
import com.example.model.StudentModel;
import com.example.service.StudentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class StudentController {
	@Autowired
	StudentService studentDAO;
	
	@RequestMapping("/")
	public String index(Model model) {
		log.info("open index page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "#", true, true));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		return "index";
	}

	
	@RequestMapping("/pengumuman/submit")
    public String searchSubmit (Model model,
            @RequestParam(value = "no-peserta", required = false) String noPeserta)
    {
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		
        StudentModel student = studentDAO.selectStudent(noPeserta);
        if (student == null) {
        	log.info("Student with nomor {} not found", noPeserta);
        	model.addAttribute("nomorPeserta", noPeserta);
        	model.addAttribute("notFound", true);
		}else{
        	log.info("Student with nomor {} found", noPeserta);
        	model.addAttribute("student", student);
        	model.addAttribute("pass", student.getKodeProdi() != null);
        	model.addAttribute("notFound", false);
		}
		return "result";
    }
	
	@RequestMapping(value="/peserta", method= RequestMethod.GET)
	public String viewProdi(
	        @RequestParam(value = "nomor", required = false, defaultValue = "0") String nomor,
	        Model model) {
		log.info("open peserta detail page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		
		StudentModel student = studentDAO.selectStudent(nomor);
		
		boolean notFound = student == null;
		model.addAttribute("notFound", notFound);
		model.addAttribute("nomor", nomor);
		model.addAttribute("student", student);
		
	    return "detail-student";
	}

}
