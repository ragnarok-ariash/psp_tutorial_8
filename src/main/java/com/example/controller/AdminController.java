package com.example.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.AjaxProdiListResponse;
import com.example.model.AjaxSuccessResponse;
import com.example.model.HeaderMenuModel;
import com.example.model.ProdiModel;
import com.example.model.ServiceDatabaseEnum;
import com.example.model.StudentModel;
import com.example.model.UniversityModel;
import com.example.service.ProdiService;
import com.example.service.StudentService;
import com.example.service.UniversityService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class AdminController {
	@Autowired
	UniversityService universityDAO;
	@Autowired
	StudentService studentDAO;
	@Autowired
	ProdiService prodiDAO;
	
	@RequestMapping("/admin")
	public String adminIndex(Model model) {
		log.info("open university list page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "#", true, true));
		model.addAttribute("headerMenus", headerMenus);
		model.addAttribute("universities", universityDAO.getAllUniversities());
		
		return "admin-menu";
	}
	
	@RequestMapping("/admin/peserta")
	public String managePeserta(Model model) {
		log.info("open university list page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		model.addAttribute("universities", universityDAO.getAllUniversities());
		
		return "peserta-admin";
	}
	
	@RequestMapping(value = "/admin/peserta/detail", 
			method = RequestMethod.GET)
	public String openPesertaDetail(Model model,
			@RequestParam(value = "no-peserta", required = false, defaultValue = "0") String noPeserta) {
		log.info("open peserta detail page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		model.addAttribute("universities", universityDAO.getAllUniversities());
		
		StudentModel student = studentDAO.selectStudentWithUnivProdi(noPeserta);
		
		boolean notFound = student == null;
		model.addAttribute("notFound", notFound);
		model.addAttribute("nomor", noPeserta);
		model.addAttribute("student", student);
		if (student != null) {
			if (student.getUniversity()!=null) 
				log.info("student univ: "+student.getUniversity().getNama());
			model.addAttribute("pass", student.getKodeProdi() != null);
		}
		
		return "detail-student-admin";

	}
	
	@RequestMapping("/admin/univ")
	public String manageUniv(Model model) {
		log.info("open university admin page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		model.addAttribute("universities", universityDAO.getAllUniversities());
		
		return "list-univ-admin";
	}
	
	@RequestMapping("/admin/univ-peserta/{kodeUniv}")
	public String getUniversityParticipant(Model model, @PathVariable(value = "kodeUniv") String kodeUniv) {
		log.info("open detail page of university with id {}", kodeUniv);
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		UniversityModel university = universityDAO.getUniversityParticipants(kodeUniv);
		
		boolean notFound = university == null;
		if (!notFound) {
			log.info("university students size: {}", university.getStudents().size());
		}
		model.addAttribute("notFound", notFound);
		model.addAttribute("kodeUniv", kodeUniv);
		model.addAttribute("university", university);
		
		return "detail-univ-peserta-admin";
	}
	
	@RequestMapping(value = "/admin/prodi", method= RequestMethod.GET)
	public String manageProdi(Model model, @RequestParam(value = "kode-univ", required = false) String kodeUniv) {
		log.info("open prodi list page for kode: {}", kodeUniv);
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		List<UniversityModel> univList = universityDAO.getAllUniversities();
		model.addAttribute("universities", univList);
		UniversityModel university = universityDAO.getUniversity(kodeUniv == null ? univList.get(0).getKode() : kodeUniv);
		model.addAttribute("university", university);
		
		return "prodi-admin";
	}
	
	@RequestMapping("/admin/prodi/{kodeProdi}")
	public String getProdiDetail(Model model, @PathVariable(value = "kodeProdi") String kodeProdi) {
		log.info("open detail page of prodi with id {}", kodeProdi);
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		
		ProdiModel prodi = prodiDAO.getProdi(kodeProdi);
		boolean noStudent = true;
		if (prodi != null && prodi.getStudents() != null) {
			log.info("prodi students size: {}", prodi.getStudents().size());
			noStudent = prodi.getStudents().size()  == 0;
			if (prodi.getStudents().size() > 0) {
				StudentModel youngestStudent = prodi.getStudents().get(0);
				StudentModel eldestStudent = prodi.getStudents().get(prodi.getStudents().size()-1);
				log.info("prodi min students age: {}", youngestStudent.getUmur());
				log.info("prodi max students age: {}", eldestStudent.getUmur());
				model.addAttribute("eldestStudent", eldestStudent);
				model.addAttribute("youngestStudent", youngestStudent);
				
			}
		}
		
		boolean notFound = prodi == null;
		model.addAttribute("noStudent", noStudent);
		model.addAttribute("notFound", notFound);
		model.addAttribute("kodeProdi", kodeProdi);
		model.addAttribute("prodi", prodi);
		
	    return "detail-prodi-admin";
	}
	
	@PostMapping("/admin/univ/getprodi")
	public ResponseEntity<?> getProdi(@Valid @RequestBody UniversityModel data, Errors errors) {
		log.info("trying to get prodi for university: {}", data.getKode());
		
		AjaxProdiListResponse result = new AjaxProdiListResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		List<ProdiModel> prodiList = prodiDAO.getAllProdiByUniv(data.getKode());
		result.setProdiList(prodiList);
		if (prodiList != null && prodiList.size() > 0) {
			result.setSuccess(true);
			result.setMessage("Success get prodi list");
		}else{
			result.setSuccess(false);
			result.setMessage("Tidak terdapat prodi pada universitas "+data.getKode());
		}
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/univ/add")
	public ResponseEntity<?> addNewUniversity(@Valid @RequestBody UniversityModel university, Errors errors) {
		log.info("trying to add new university: {}, {}, {}", 
				university.getKode(), university.getNama(), university.getUrl());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		boolean success = false;
		String errorMsg = "Error Unknown";
		ServiceDatabaseEnum databaseEnum = universityDAO.addUniversity(university);
		switch (databaseEnum) {
			case SUCCESS:
				success = true;
				break;
			case FAILED_INSERT_UNIV_DUPLICATE_KODE:
				errorMsg = "Univ with kode: "+university.getKode()+" already exist";
				break;
			case ERROR_UNIV_KODE_CHARLIMIT:
				errorMsg = "Kode should not exceed 2 characters";
				break;
			case ERROR_UNIV_NAMA_CHARLIMIT:
				errorMsg = "Nama should not exceed 100 characters";
				break;
			case ERROR_UNIV_URL_CHARLIMIT:
				errorMsg = "URL should not exceed 100 characters";
				break;

			default:
				break;
		}
		result.setSuccess(success);
		if (!success) {
			result.setMessage(errorMsg);
		}else{
			result.setMessage("Successfully add new university");
		}
		
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/univ/update")
	public ResponseEntity<?> updateUniversity(@Valid @RequestBody UniversityModel university, Errors errors) {
		log.info("trying to update university: {}, {}, {}", 
				university.getKode(), university.getNama(), university.getUrl());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		boolean success = false;
		String errorMsg = "Error Unknown";
		ServiceDatabaseEnum databaseEnum = universityDAO.updateUniversity(university);
		switch (databaseEnum) {
			case SUCCESS:
				success = true;
				break;
			case FAILED_INSERT_UNIV_DUPLICATE_KODE:
				errorMsg = "Univ with kode: "+university.getKode()+" already exist";
				break;
			case ERROR_UNIV_KODE_CHARLIMIT:
				errorMsg = "Kode should not exceed 2 characters";
				break;
			case ERROR_UNIV_NAMA_CHARLIMIT:
				errorMsg = "Nama should not exceed 100 characters";
				break;
			case ERROR_UNIV_URL_CHARLIMIT:
				errorMsg = "URL should not exceed 100 characters";
				break;
	
			default:
				break;
		}
		result.setSuccess(success);
		if (!success) {
			result.setMessage(errorMsg);
		}else{
			result.setMessage("University has been saved successfully");
		}
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/univ/delete")
	public ResponseEntity<?> deleteUniversity(@Valid @RequestBody UniversityModel university, Errors errors) {
		log.info("trying to delete university: {} {} {}", 
				university.getKode(), university.getNama(), university.getUrl());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		universityDAO.deleteUniversity(university.getKode());
		result.setSuccess(true);
		result.setMessage("University has been deleted successfully");
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/peserta/add")
	public ResponseEntity<?> addNewPeserta(@Valid @RequestBody StudentModel student, Errors errors) {
		log.info("trying to add new student: {}, {}, {}", 
				student.getNomor(), student.getNama(), student.getTglLahir());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		boolean success = false;
		String errorMsg = "Error Unknown";
		ServiceDatabaseEnum databaseEnum = studentDAO.addStudent(student);
		switch (databaseEnum) {
			case SUCCESS:
				success = true;
				break;
			case FAILED_INSERT_PESERTA_DUPLICATE_KODE:
				errorMsg = "Student with kode peserta: "+student.getNomor()+" already exist";
				break;
			case ERROR_PESERTA_NOMOR_CHARLIMIT:
				errorMsg = "Nomor should not exceed 10 characters";
				break;
			case ERROR_PESERTA_NAMA_CHARLIMIT:
				errorMsg = "Nama should not exceed 30 characters";
				break;
			case ERROR_PESERTA_KODEPRODI_CHARLIMIT:
				errorMsg = "Invalid kode prodi";
				break;

			default:
				break;
		}
		result.setSuccess(success);
		if (!success) {
			result.setMessage(errorMsg);
		}else{
			result.setMessage("Successfully add new student");
		}
		
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/peserta/update")
	public ResponseEntity<?> updatePeserta(@Valid @RequestBody StudentModel student, Errors errors) {
		log.info("trying to add new student: {}, {}, {}", 
				student.getNomor(), student.getNama(), student.getTglLahir());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		boolean success = false;
		String errorMsg = "Error Unknown";
		ServiceDatabaseEnum databaseEnum = studentDAO.updateStudent(student);
		switch (databaseEnum) {
			case SUCCESS:
				success = true;
				break;
			case FAILED_INSERT_PESERTA_DUPLICATE_KODE:
				errorMsg = "Student with kode peserta: "+student.getNomor()+" already exist";
				break;
			case ERROR_PESERTA_NOMOR_CHARLIMIT:
				errorMsg = "Nomor should not exceed 10 characters";
				break;
			case ERROR_PESERTA_NAMA_CHARLIMIT:
				errorMsg = "Nama should not exceed 30 characters";
				break;
			case ERROR_PESERTA_KODEPRODI_CHARLIMIT:
				errorMsg = "Invalid kode prodi";
				break;
	
			default:
				break;
		}
		result.setSuccess(success);
		if (!success) {
			result.setMessage(errorMsg);
		}else{
			result.setMessage("Student has been saved successfully");
		}
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/peserta/delete")
	public ResponseEntity<?> deletePeserta(@Valid @RequestBody StudentModel student, Errors errors) {
		log.info("trying to delete student: {}", student.getNomor());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		studentDAO.deleteStudent(student.getNomor());
		result.setSuccess(true);
		result.setMessage("Student has been deleted successfully");
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/prodi/add")
	public ResponseEntity<?> addNewProdi(@Valid @RequestBody ProdiModel prodi, Errors errors) {
		log.info("trying to add new prodi: {}, {}, {}", 
				prodi.getKodeUniv(), prodi.getKode(), prodi.getNama());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		boolean success = false;
		String errorMsg = "Error Unknown";
		ServiceDatabaseEnum databaseEnum = prodiDAO.addProdi(prodi);
		switch (databaseEnum) {
			case SUCCESS:
				success = true;
				break;
			case FAILED_INSERT_PRODI_DUPLICATE_KODE:
				errorMsg = "Prodi with kode prodi: "+prodi.getKode()+" already exist";
				break;
			case ERROR_PRODI_KODE_CHARLIMIT:
				errorMsg = "Nomor should not exceed 6 characters";
				break;
			case ERROR_PRODI_NAMA_CHARLIMIT:
				errorMsg = "Nama should not exceed 100 characters";
				break;
			case ERROR_PRODI_KODEUNIV_CHARLIMIT:
				errorMsg = "Invalid kode univ";
				break;
	
			default:
				break;
		}
		result.setSuccess(success);
		if (!success) {
			result.setMessage(errorMsg);
		}else{
			result.setMessage("Successfully add new prodi: "+prodi.getNama());
		}
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/prodi/update")
	public ResponseEntity<?> updateProdi(@Valid @RequestBody ProdiModel prodi, Errors errors) {
		log.info("trying to update prodi: {}, {}, {}", 
				prodi.getKodeUniv(), prodi.getKode(), prodi.getNama());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		boolean success = false;
		String errorMsg = "Error Unknown";
		ServiceDatabaseEnum databaseEnum = prodiDAO.updateProdi(prodi);
		switch (databaseEnum) {
			case SUCCESS:
				success = true;
				break;
			case FAILED_INSERT_PRODI_DUPLICATE_KODE:
				errorMsg = "Prodi with kode prodi: "+prodi.getKode()+" already exist";
				break;
			case ERROR_PRODI_KODE_CHARLIMIT:
				errorMsg = "Nomor should not exceed 6 characters";
				break;
			case ERROR_PRODI_NAMA_CHARLIMIT:
				errorMsg = "Nama should not exceed 100 characters";
				break;
			case ERROR_PRODI_KODEUNIV_CHARLIMIT:
				errorMsg = "Invalid kode univ";
				break;
	
			default:
				break;
		}
		result.setSuccess(success);
		if (!success) {
			result.setMessage(errorMsg);
		}else{
			result.setMessage("Prodi has been saved successfully");
		}
		
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/admin/prodi/delete")
	public ResponseEntity<?> deleteProdi(@Valid @RequestBody ProdiModel prodi, Errors errors) {
		log.info("trying to delete prodi: {}", prodi.getNama());
		
		AjaxSuccessResponse result = new AjaxSuccessResponse();
		//If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			result.setMessage(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
			result.setSuccess(false);
			
			return ResponseEntity.badRequest().body(result);
		}
		
		prodiDAO.deleteProdi(prodi.getKode(), prodi.getKodeUniv());
		result.setSuccess(true);
		result.setMessage("Prodi has been deleted successfully");
		
		return ResponseEntity.ok(result);
	}

}
