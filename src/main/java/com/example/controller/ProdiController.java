package com.example.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.HeaderMenuModel;
import com.example.model.ProdiModel;
import com.example.model.StudentModel;
import com.example.service.ProdiService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ProdiController {
	@Autowired
	ProdiService prodiDAO;
	
	@RequestMapping(value="/prodi", method= RequestMethod.GET)
	public String viewProdi(
	        @RequestParam(value = "kode", required = false, defaultValue = "0") String idProdi,
	        Model model) {
		log.info("open prodi detail page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		
		ProdiModel prodi = prodiDAO.getProdi(idProdi);
		boolean noStudent = true;
		if (prodi != null) {
			log.info("prodi students size: {}", prodi.getStudents().size());
			noStudent = prodi.getStudents().size()  == 0;
			if (prodi.getStudents().size() > 0) {
				StudentModel youngestStudent = prodi.getStudents().get(0);
				StudentModel eldestStudent = prodi.getStudents().get(prodi.getStudents().size()-1);
				log.info("prodi min students age: {}", youngestStudent.getUmur());
				log.info("prodi max students age: {}", eldestStudent.getUmur());
				model.addAttribute("eldestStudent", eldestStudent);
				model.addAttribute("youngestStudent", youngestStudent);
				
			}
		}
		
		boolean notFound = prodi == null;
		model.addAttribute("noStudent", noStudent);
		model.addAttribute("notFound", notFound);
		model.addAttribute("kodeProdi", idProdi);
		model.addAttribute("prodi", prodi);
		
	    return "detail-prodi";
	}
}
