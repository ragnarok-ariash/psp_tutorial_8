package com.example.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.HeaderMenuModel;
import com.example.model.UniversityModel;
import com.example.service.UniversityService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class UniversityController {
	@Autowired
	UniversityService universityDAO;
	
	@RequestMapping("/univ")
	public String getAll(Model model) {
		log.info("open university list page");
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "#", true, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		model.addAttribute("universities", universityDAO.getAllUniversities());
		
		return "list-univ";
	}
	
	@RequestMapping("/univ/{kodeUniv}")
	public String getUniversity(Model model, @PathVariable(value = "kodeUniv") String kodeUniv) {
		log.info("open detail page of university with id {}", kodeUniv);
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		UniversityModel university = universityDAO.getUniversity(kodeUniv);
		
		boolean notFound = university == null;
		if (!notFound && university.getProdiList() != null) {
			log.info("university prodi size: {}", university.getProdiList().size());
		}
		model.addAttribute("notFound", notFound);
		model.addAttribute("kodeUniv", kodeUniv);
		model.addAttribute("university", university);
		
		return "detail-univ";
	}
	
	@RequestMapping("/univ-peserta/{kodeUniv}")
	public String getUniversityParticipant(Model model, @PathVariable(value = "kodeUniv") String kodeUniv) {
		log.info("open detail page of university with id {}", kodeUniv);
		ArrayList<HeaderMenuModel> headerMenus = new ArrayList<>();
		headerMenus.add(new HeaderMenuModel("Home", "/", false, false));
		headerMenus.add(new HeaderMenuModel("Universitas", "/univ", false, false));
		headerMenus.add(new HeaderMenuModel("Admin", "/admin", false, false));
		model.addAttribute("headerMenus", headerMenus);
		UniversityModel university = universityDAO.getUniversityParticipants(kodeUniv);
		
		boolean notFound = university == null;
		if (!notFound && university.getStudents() != null) {
			log.info("university students size: {}", university.getStudents().size());
		}
		model.addAttribute("notFound", notFound);
		model.addAttribute("kodeUniv", kodeUniv);
		model.addAttribute("university", university);
		
		return "detail-univ-peserta";
	}

}
