package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.ProdiModel;
import com.example.model.StudentModel;
import com.example.model.UniversityModel;

@Mapper
public interface UniversityMapper {
	
	@Select("select * from univ")
	@Results(value = {
			@Result(property="kode", column="kode_univ"),
			@Result(property="nama", column="nama_univ"),
			@Result(property="url", column="url_univ")
	})
	List<UniversityModel> getAllUniversities();
	
	@Select("select * from univ where kode_univ = #{kode}")
	@Results(value = {
			@Result(property="kode", column="kode_univ"),
			@Result(property="nama", column="nama_univ"),
			@Result(property="url", column="url_univ"),
			@Result(property="prodiList", column="kode_univ", 
			javaType = List.class, many = @Many(select="getProdi"))
	})
	UniversityModel getUniversity(@Param("kode") String id);
	
	@Select("select * from univ where kode_univ = #{kode}")
	@Results(value = {
			@Result(property="kode", column="kode_univ"),
			@Result(property="nama", column="nama_univ"),
			@Result(property="url", column="url_univ"),
			@Result(property="students", column="kode_univ", 
			javaType = List.class, many = @Many(select="getStudents"))
	})
	UniversityModel getUniversityParticipants(@Param("kode") String id);
	
	@Insert("INSERT INTO univ (kode_univ, nama_univ, url_univ) VALUES (#{kode}, #{nama}, #{url})")
	void addUniversity(UniversityModel university);

	@Delete("Delete from univ where kode_univ = #{kode}")
	void deleteUniversity(@Param("kode") String kodeUniv);

	@Update("Update univ set nama_univ = #{university.nama}, url_univ = #{university.url} where kode_univ = #{university.kode}")
	void updateUniversity(@Param("university") UniversityModel university);
	
	@Select("select prodi.* "
	+"from prodi join univ "
	+"on prodi.kode_univ = univ.kode_univ "
	+"where univ.kode_univ=#{kodeUniv}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kode", column="kode_prodi"),
			@Result(property="nama", column="nama_prodi")
	})
	List<ProdiModel> getProdi(@Param("kodeUniv") String kodeUniv);
	
	@Select("SELECT A.*, P.nama_prodi, U.nama_univ, U.url_univ "
			+ "FROM peserta A "
			+ "JOIN prodi P ON A.kode_prodi = P.kode_prodi "
			+ "JOIN univ U ON P.kode_univ = U.kode_univ "
			+ "where U.kode_univ = #{kodeUniv} order by A.nomor")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ"),
			@Result(property="umur", column="age")
	})
	List<StudentModel> getStudents(@Param("kodeUniv") String kodeUniv);

}
