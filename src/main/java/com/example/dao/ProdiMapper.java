package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.ProdiModel;
import com.example.model.StudentModel;

@Mapper
public interface ProdiMapper {
	
	@Select("select prodi.*, univ.nama_univ, univ.url_univ from prodi "
			+ "join univ "
			+ "on prodi.kode_univ = univ.kode_univ")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kode", column="kode_prodi"),
			@Result(property="nama", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ"),
			@Result(property="students", column="kode_prodi", 
			javaType = List.class, many = @Many(select="getStudents"))
	})
	List<ProdiModel> getAllProdi();
	
	@Select("select prodi.*, univ.nama_univ, univ.url_univ "
			+ "from prodi "
			+ "join univ "
			+ "on prodi.kode_univ = univ.kode_univ "
			+ "where kode_prodi = #{kodeProdi}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kode", column="kode_prodi"),
			@Result(property="nama", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ"),
			@Result(property="students", column="kode_prodi", 
			javaType = List.class, many = @Many(select="getStudents"))
	})
	ProdiModel getProdi(@Param("kodeProdi") String idProdi);
	
	@Select("select prodi.*, univ.nama_univ, univ.url_univ "
			+ "from prodi "
			+ "join univ "
			+ "on prodi.kode_univ = univ.kode_univ "
			+ "where univ.kode_univ = #{kodeUniv}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kode", column="kode_prodi"),
			@Result(property="nama", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	List<ProdiModel> getAllProdiByUniv(@Param("kodeUniv") String kodeUniv);
	
	@Insert("INSERT INTO prodi (kode_univ, kode_prodi, nama_prodi) VALUES (#{kodeUniv}, #{kode}, #{nama})")
	void addProdi(ProdiModel prodi);

	@Delete("Delete from prodi where kode_prodi = #{kode} and kode_univ = #{kodeUniv}")
	void deleteProdi(@Param("kode") String kodeProdi, @Param("kodeUniv") String kodeUniv);

	@Update("Update prodi set nama_prodi = #{prodi.nama}, kode_univ = #{prodi.kodeUniv} where kode_prodi = #{prodi.kode}")
	void updateProdi(@Param("prodi") ProdiModel prodi);
	
	@Select("select peserta.*, TIMESTAMPDIFF(YEAR,tgl_lahir,CURDATE()) AS age "
	+"from peserta join prodi "
	+"on peserta.kode_prodi = prodi.kode_prodi "
	+"where prodi.kode_prodi=#{kodeProdi} "
	+"order by age")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="umur", column="age")
	})
	List<StudentModel> getStudents(@Param("kodeProdi") String kodeProdi);

}
