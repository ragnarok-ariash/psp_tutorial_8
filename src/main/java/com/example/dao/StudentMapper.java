package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.One;

import com.example.model.ProdiModel;
import com.example.model.StudentModel;
import com.example.model.UniversityModel;

@Mapper
public interface StudentMapper {
	@Select("SELECT A.*, P.nama_prodi, U.nama_univ, U.url_univ, TIMESTAMPDIFF(YEAR,tgl_lahir,CURDATE()) AS age "
			+ "FROM peserta A "
			+ "LEFT JOIN prodi P ON A.kode_prodi = P.kode_prodi "
			+ "LEFT JOIN univ U ON P.kode_univ = U.kode_univ "
			+ "where A.nomor = #{nomor}")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ"),
			@Result(property="umur", column="age")
	})
	StudentModel selectStudent(@Param("nomor") String nomor);
	
	@Select("SELECT A.*, U.kode_univ, TIMESTAMPDIFF(YEAR,tgl_lahir,CURDATE()) AS age "
			+ "FROM peserta A "
			+ "LEFT JOIN prodi P ON A.kode_prodi = P.kode_prodi "
			+ "LEFT JOIN univ U ON P.kode_univ = U.kode_univ "
			+ "where A.nomor = #{nomor}")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="umur", column="age"),
			@Result(property = "university", column = "kode_univ", 
			javaType = UniversityModel.class, one = @One(select="getUniversity")),
	})
	StudentModel selectStudentWithUnivProdi(@Param("nomor") String nomor);
	
	@Insert("INSERT INTO peserta (nomor, nama, tgl_lahir, kode_prodi) VALUES (#{nomor}, #{nama}, #{tglLahir}, #{kodeProdi})")
	void addStudent(StudentModel student);

	@Delete("Delete from peserta where nomor = #{nomor}")
	void deleteStudent(@Param("nomor") String nomorPeserta);

	@Update("Update peserta "
			+ "set nama = #{student.nama}, "
			+ "tgl_lahir = #{student.tglLahir}, "
			+ "kode_prodi = #{student.kodeProdi} "
			+ "where nomor = #{student.nomor}")
	void updateStudent(@Param("student") StudentModel student);
	
	@Select("select * from univ where kode_univ = #{kode}")
	@Results(value = {
			@Result(property="kode", column="kode_univ"),
			@Result(property="nama", column="nama_univ"),
			@Result(property="url", column="url_univ"),
			@Result(property="prodiList", column="kode_univ", 
			javaType = List.class, many = @Many(select="getProdi"))
	})
	UniversityModel getUniversity(@Param("kode") String kodeUniv);
	
	@Select("select prodi.* "
	+"from prodi join univ "
	+"on prodi.kode_univ = univ.kode_univ "
	+"where univ.kode_univ=#{kodeUniv}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kode", column="kode_prodi"),
			@Result(property="nama", column="nama_prodi")
	})
	List<ProdiModel> getProdi(@Param("kodeUniv") String kodeUniv);
	
}
