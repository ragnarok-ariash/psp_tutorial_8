package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HeaderMenuModel {
	private String title;
	private String ref;
	private boolean active;
	private boolean scroll;
}
