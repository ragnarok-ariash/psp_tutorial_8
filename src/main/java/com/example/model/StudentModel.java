package com.example.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentModel
{
    private String nomor;
    private String nama;
    private Date tglLahir;
    private String kodeProdi;
    private String namaProdi;
    private String namaUniv;
    private String urlUniv;
    private int umur;
    private ProdiModel prodi;
    private UniversityModel university;
}
