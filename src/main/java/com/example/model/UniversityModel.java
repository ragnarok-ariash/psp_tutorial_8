package com.example.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniversityModel {
	private String kode;
	private String nama;
	private String url;
	private List<ProdiModel> prodiList;
	private List<StudentModel> students;
}
